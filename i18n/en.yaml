home.header:
  other: Opt Green
home.header-subtitle:
  other: Independent, energy-efficient Free & Open Source Software for long-term hardware use.
home.sustainability-title:
  other: Be Green (End User)
home.sustainability-description:
  other: Opt in to lower energy demands, extended hardware life, device independence, and user control for a healthier digital society.
home.advocate-title:
  other: Grow Green (Advocate)
home.advocate-description:
  other: Build communities and user support networks to bring the benefits of independent, sustainable software to your hometown.
home.impact-title:
  other: Make Green (Dev)
home.impact-description:
  other: Develop applications and software features with a focus on efficiency and sustainability for this and future generations.
home.about-foss-title:
  other: What makes FOSS sustainable?
home.featured-blog-posts-title:
  other: Featured blog posts
home.events-title:
  other: Events
home.about-foss-description:  
  other : Free & Open Source Software (FOSS) guarantees transparency and user autonomy, by design. These are not features, but inherent qualities to open development and software licensing. As a result, software and hardware are no longer dependent on vendors for support.
home.about-foss-description-2:  
  other : FOSS can be studied and improved independently, including in its energy consumption. Premature hardware obsolescence is a thing of the past. FOSS runs on devices that are decades old. Keep your computer in use to the end of the hardware operating life, not the software.
home.about-kde-title: 
  other : About KDE and KDE Eco
home.about-kde-description-kde:
  other :  "KDE is a world-wide community of software engineers, artists, writers, translators, and creators who are committed to Free Software development. We are a cooperative enterprise: we work together to achieve the common goal of building the world's finest Free & Open Source Software. Everyone is welcome to join and contribute to KDE, including you."
home.about-kde-description-kde-2:
  other : Supported by the KDE community since 2021, KDE Eco has the ongoing mission of the development and adoption of sustainable Free & Open Source Software worldwide. As the ecological impact of digitization grows more relevant, we continue to raise awareness about the role of software design in reducing environmental harm. We are proudly a part of a growing number of communities and organizations working to have a positive ecological impact, today.
home.header.sustainable-goal.description:
 other: Software has a direct affect on energy and resource consumption. Since 2022, KDE has the goal of providing software which does this in a way that reduces software’s environmental impact for us and future generations.
home.header.feep.subtitle:
  other: Free and open source software Energy Efficiency Project
home.header.feep.description:
 other: How can you measure software’s energy consumption? And does it make a difference? We are working to make free software more sustainable.
home.header.be4foss.subtitle:
  other: Blauer Engel for Free and Open Source Software
home.header.be4foss.description:
  other: Eco-labels for products are used widely already. But how can they be transferred to software? What criteria is relevant? We need more pioneers to find out how we can make software sustainable.
home.header.more:
  other: Learn More
home.eco-handbook:
  other: KDE Eco Handbook
home.about-website:
  other: About This Website
home.about-website-description:
  other : 'This website is open-source. Source code and asset licenses can be found in the [KDE Eco Repository](https://invent.kde.org/websites/eco-kde-org). This website was checked by the [greenwebfoundation.org](https://www.thegreenwebfoundation.org/green-web-check/?url=https%3A%2F%2Feco.kde.org) and is labelled "Hosted Green". Website is hosted by: Hetzner Online AG. Supporting evidence for this hoster’s claims: [Sustainability Page](https://www.hetzner.com/unternehmen/umweltschutz).'
home.eco-handbook.alt:
  other: Plots of real time measurements using KDE’s LabPlot.
home.okular:
  other: Okular Awarded Blue Angel Ecolabel
home.okular.be:
  other: In 2022, KDE’s popular multi-platform PDF reader and universal document viewer Okular became the first ever eco-certified computer program!
home.okular.pr:
  other: Read More
home.engage:
  other: "Get Involved"
home.engage.joinus:
  other: "Join us in the sustainable software development movement!"
home.engage.repo:
  other: Repositories
home.engage.matrix:
  other: Chat
home.engage.mailinglist:
  other: Mailing List
home.sustainable-goal:
  other: KDE’s Sustainable Software Goal
home.sustainable-goal.alt:
  other: Image of computer directory with globe
home.feep:
  other: FOSS Energy Efficiency Project (FEEP)
home.feep.alt:
  other: Image of globe with computer mouse
home.be4foss:
  other: Blauer Engel For FOSS (BE4FOSS)
home.be4foss.alt:
  other: Image of Blauer Engel ecolabel
home.greenweb:
  other: This Website Is Hosted Green
home.greenweb.alt:
  other: Image of Green Web Foundation label
home.funding.alt:
  other: Image of BMUV/UBA
home.button.see-all:
  other: See all
blog.categories:
  other: Categories
read-more:
  other: Read more
view-all-posts:
  other: View all posts
posts:
  other: News from the blog

be-green.header:
  other: Be Green
be-green.header-subtitle:
  other : Install Free & Open Source Software 
be-green.opt-free.title:
  other: Opt Free with FOSS Apps
be-green.opt-free.description:
  other: Install sustainable apps on any computer and operating system.
be-green.set-free.title:
  other: Set Free with Old Hardware
be-green.set-free.description:
  other: Vendors control and kill support for aging hardware, by design.
be-green.be-free.title:
  other: Be Free with New Hardware
be-green.be-free.description:
  other: Switch programs when needed for software that is sustainable for users and the environment.
be-green.opt-free-section.title:
  other: Opt Free with FOSS Apps
be-green.opt-free-section.description:
  other: Using Free & Open Source software applications gives users flexibility. You can use older hardware for longer when you have control over software's demands on resources. Switch programs when needed and choose software that is more sustainable for users and for the environment.
be-green.opt-free-section.button:
  other: See All
be-green.opt-free-section.creators.title:
  other: KDE for Creators
be-green.opt-free-section.creators.description:
  other: Creative software tools tailored for artists, designers, and media creators.
be-green.opt-free-section.gamers.title:
  other: KDE for Gamers
be-green.opt-free-section.gamers.description:
  other: Powerful gaming applications for an optimized and immersive gaming experience.
be-green.opt-free-section.students.title:
  other: KDE for Students
be-green.opt-free-section.students.description:
  other: Educational software that provides flexible and user-friendly tools for students.
be-green.set-free-section.title:
  other: Set Free Your Old Hardware
be-green.set-free-section.description:
  other: Installing a Free & Open Source operating system takes just 3 steps and the time needed to enjoy a couple cups of tea.
be-green.set-free-section.download-title:
  other: Download a FOSS Operating System
be-green.set-free-section.download-description:
  other: Download the FOSS operating system you want to install. For devices less than 10 years old, Fedora Linux with KDE spin is our recommendation.
be-green.set-free-section.flash-title:
  other: Flash Operating System to a USB Stick
be-green.set-free-section.flash-description:
  other: With one of several easy-to-use tools, flash the operating system to a USB stick. This will overwrite all data on the USB stick, so make sure to back up the files you want to keep.
be-green.set-free-section.boot-title:
  other: Boot and Install on Computer
be-green.set-free-section.boot-description:
  other: Boot your computer from USB – you will need to know the correct keyboard combination beforehand. Click "Install" and follow the instructions. Voila! You have set your computer free. Make sure you have backed up any data you want to keep first.
be-green.set-free-section.button:
  other: Learn More
be-green.be-free-section.title:
  other: Be Free with New Hardware
be-green.be-free-section.description:
  other: KDE partners with several hardware providers to give the best experience out-of-the-box. Because these devices support Free & Open Source Software, you can use them however you want, for as long as you want. Support for repairability is a focus for the recommended devices featured here!
be-green.be-free-section.slimbook-title:
  other: Slimbook
be-green.be-free-section.slimbook-description:
  other: With a FOSS operating system pre-installed and a large catalog of spare parts, your Slimbook can remain in use for years to come.
be-green.be-free-section.slimbook-button:
  other: Learn more
be-green.be-free-section.framework-title:
  other: Framework
be-green.be-free-section.framework-description:
  other: By designing hardware to be modular and customizable, a Framework computer is as easy to repair as snapping in a new part.
be-green.be-free-section.framework-button:
  other: Learn more
make-green.header:
  other: Make Green
make-green.header-subtitle:
  other: Develop Energy-Efficient Free & Open Source Software 
make-green.measure.title:
  other: Measure Software Energy Use
make-green.measure.description:
  other: Free & Open Source Software has always been about transparency – let's extend that transparency to software's energy demands.
home.learn-more:
  other: Learn More
make-green.read.title:
  other: Read the KDE Eco Handbook
make-green.read.description:
  other: '"Applying The Blue Angel Criteria To Free Software" details how to eco-certify software and why it matters.'
make-green.prepare.title:
  other: Prepare Scripts to Measure Energy Use
make-green.prepare.description:
  other: Energy consumption measurements require a scripted and replicable workflow of actions.
make-green.measure-section.title:
  other: Measure Your Software's Energy Use
make-green.measure-section.description:
  other: Setting up a lab is as easy as buying an external power meter and following the lab setup guide in the KDE Eco handbook. KEcoLab makes the process even easier by enabling developers to remotely measure the energy consumption of their software using GitLab's CI/CD pipeline. What's more, at the end of the measurement process you will have an energy consumption report ready for Blue Angel eco certification.
make-green.measure-section.button:
  other: View Repository
make-green.handbook.title:
  other: Use the KDE Eco Handbook
make-green.handbook.description:
  other: The handbook "Applying The Blue Angel Criteria To Free Software" gives an overview of the environmental harm driven by software and how the Blue Angel ecolabel — the official environmental label of the German government — provides a benchmark for sustainable software design. Learn how Free & Open Source Software's values of transparency and user autonomy foster sustainability, as well as how to set up a dedicated lab to measure your software's energy consumption.
make-green.handbook.button:
  other: Read More
make-green.script-section.title:
  other: Prepare Scripts to Measure Energy Use
make-green.script-section.description:
  other: Emulate user behavior for easy replication of your measurements. That's good science! KdeEcoTest and KdeEcoTestCreator are Python-based tools designed for reproducing computer actions by simulating keyboard and mouse activity. The tools work on GNU/Linux and Microsoft Windows.
make-green.script-section.button:
  other: View Repository
make-green.past-projects-section.title:
  other: Past Projects
make-green.past-projects-section.sustainable-goal.title:
  other: KDE's Sustainable Software Goal
make-green.past-projects-section.feep.title:
  other: FOSS Energy Efficiency Project (FEEP)
make-green.past-projects-section.be4foss.title:
  other: Blauer Engel For FOSS (BE4FOSS)
