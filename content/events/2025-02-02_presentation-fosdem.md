---
author: KDE Eco
date: 2025-02-02
title: "Presentation at FOSDEM 2025"
thumbnail: "/events/images/2025-02-02_event-fosdem-presentation.png"
location: "Berlin, DE"
featured: true
lang: "en"
---
